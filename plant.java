package plant;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

class plant {

    static void penampungInput() throws InterruptedException{
        int umurTanaman = 0;
        while (true) {
            Scanner input = new Scanner(System.in);
            System.out.print("\n[Tanaman Bahagia v0.01]\n");
            System.out.print("- Menyiram tanaman [1]\n");
            System.out.print("- Cek umur tanaman [2]\n");
            System.out.print("- Keluar program [9] \n");
            System.out.print("Input : ");
            int nomerinput = input.nextInt();
            switch (nomerinput) {
                case 1:
                umurTanaman = umurTanaman + 1;
                    watering.wateringImage();
                    break;
                case 2:
                    System.out.print("\nUmur tanaman : " + umurTanaman + "x Siram");
                    if (umurTanaman <= 5) {
                        watering.tanamanKecil();  
                    } else if (umurTanaman > 5) {
                        watering.tanamanBesar();
                    }
                    break;
                case 9:
                    System.out.print("Exiting..");
                    System.exit(0);
            }
            TimeUnit.SECONDS.sleep(2);
        }
    }
    public static void main(String[] args) throws InterruptedException {
        penampungInput();
    }
}
